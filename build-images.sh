#!/usr/bin/env bash
# Usage:
#   build-images [dry-run|publish]
#
# Builds all images that have changed.
# Images are named according to their directory name and versioned according to the date and time of building.
# Adapted from https://code.videolan.org/videolan/docker-images/commit/d538ad1e98784ad4283a33efff586bf9ce9121f7

set -e

G_OAUTH_URL='https://gitlab.com/jwt/auth'

G_MODE="dry-run"
if [[ "${1}" = "publish" ]]; then
    G_MODE="publish"
fi

get_local_image_version() {
    local DOCKERFILE="${1}"
    grep -E 'LABEL\s+version=' ${DOCKERFILE} | cut -d'=' -f2
}

get_registry_bearer_token_for_images() {
    local IMAGES=("$@")
    local BASE64_AUTH=$(echo -n "${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}" | base64)

    local URL="${G_OAUTH_URL}?client_id=docker&offline_token=true&service=container_registry"
    for IMAGE in "${IMAGES[@]}"; do
        URL="${URL}&scope=repository%3A${CI_PROJECT_PATH//\//%2F}%2F${IMAGE}%3Apull"
    done

    local RESPONSE=$(curl --silent --request GET --url "${URL}" --header "Authorization: Basic ${BASE64_AUTH}")
    echo ${RESPONSE} | jq --raw-output --compact-output '.token'
}

get_remote_image_tags() {
    local IMAGE_NAME="${1}"
    local BEARER_TOKEN="${2}"

    local RESPONSE=$(curl --silent --url "https://${CI_REGISTRY}/v2/${CI_PROJECT_PATH}/${IMAGE_NAME}/tags/list" --header "Authorization: Bearer ${BEARER_TOKEN}")
    local ERRORS=$(echo "${RESPONSE}" | jq --raw-output --compact-output '.errors')
    if [[ -z "${RESPONSE}" ]]; then
        echo "Error fetching remote tags."
        return 1
    fi
    if [[ "${ERRORS}" != 'null' ]]; then
        echo "${ERRORS}"
        return 1
    fi

    local TAGS=$(echo "${RESPONSE}" | jq --raw-output --compact-output '.tags | .[]')
    echo "${TAGS[@]}"
}

remote_image_tag_exists() {
    local IMAGE_NAME="${1}"
    local TAG="${2}"
    local BEARER_TOKEN="${3}"

    local REMOTE_TAGS=($(set -e; get_remote_image_tags "${IMAGE_NAME}" "${BEARER_TOKEN}"))
    for T in "${REMOTE_TAGS[@]}"; do
        if [[ "${T}" = "${TAG}" ]]; then
            return 0
        fi
    done
    return 1
}

docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"

G_DOCKERFILES=$(find . \( \( -name '*.dockerfile' -or -name 'Dockerfile' \) -and ! -name 'win*.dockerfile' \) -print | sort)

while read DOCKERFILE; do
    G_IMAGE_NAME="$(basename "$(dirname "${DOCKERFILE}")")"
    G_IMAGE_SUFFIX="$(basename "${DOCKERFILE}" ".dockerfile")"
    G_IMAGE_VERSION=$(set -e; get_local_image_version "${DOCKERFILE}")

    if [[ "${G_IMAGE_SUFFIX}" != "Dockerfile" ]]; then
        G_IMAGE_SUFFIX="-${G_IMAGE_SUFFIX}"
    else
        G_IMAGE_SUFFIX=""
    fi

    if [[ -z "${G_IMAGE_VERSION}" ]]; then
        echo "Version label is required for image: ${G_IMAGE_NAME}"
        exit 1
    fi

    G_IMAGE_TAG="${G_IMAGE_VERSION}${G_IMAGE_SUFFIX}"
    G_DOCKER_API_BEARER_TOKEN=$(set -e; get_registry_bearer_token_for_images "${G_IMAGE_NAME}")
    if remote_image_tag_exists "${G_IMAGE_NAME}" "${G_IMAGE_TAG}" "${G_DOCKER_API_BEARER_TOKEN}"; then
        echo ""
        echo "======================================================================================"
        echo "Skipping build of ${G_IMAGE_NAME}:${G_IMAGE_TAG} since remote image already exists"
        echo "======================================================================================"
        echo ""
        continue
    fi

    G_BUILD_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${G_IMAGE_NAME}:${G_IMAGE_TAG}"

    echo ""
    echo "======================================================================================"
    echo "Building ${G_BUILD_IMAGE_NAME}"

    if [[ "${G_MODE}" = "publish" ]]; then
        docker build --tag "${G_BUILD_IMAGE_NAME}" --file "${DOCKERFILE}" "${G_IMAGE_NAME}/"
        docker push "${G_BUILD_IMAGE_NAME}"
    else
        docker build --no-cache --tag "${G_BUILD_IMAGE_NAME}" --file "${DOCKERFILE}" "${G_IMAGE_NAME}/"
    fi
    echo "======================================================================================"
    echo ""
done <<< "${G_DOCKERFILES}"

docker logout "${CI_REGISTRY}"
