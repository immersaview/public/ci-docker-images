FROM ubuntu:focal-20210609

LABEL version=1.0.0

ARG DEBIAN_FRONTEND=noninteractive

## Setting the SHELL environment variable as this is what flock uses for it's commands internally.
## This is normally set on Ubuntu machines.  Using the SHELL docker cmd or other methods has not worked.

## Most likely this is because setting SHELL via ENV sets it in the environment in the image permanently so it's always
## set to bash and will likely then be able to handle bash-isms in various build scripts rather than the system default
## dash shell.  The SHELL dockerfile command just sets the shell for the next command in the sequence.

## TZ is needed to override the prompting of timezone in tzdata configuration which is being pulled in by
## one of the packages.
ENV SHELL="/bin/bash" TZ="Australia/Brisbane"

## nuget leaves a lock file, which is what the line 'rm -rf /tmp/NuGetScratch/lock/*' removes.

## We need the initial kitware signing key to prime the pump for cmake update.
## The expected flow would then be to delete the initial key and install the kitware-archive-keyring package for auto-updates
## but for an image that shouldn't be necessary.
RUN apt-get update \
    && apt-get install -y gnupg apt-transport-https ca-certificates gpg wget \
    && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF \
    && wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null \
    && echo "deb https://download.mono-project.com/repo/ubuntu stable-focal main" | tee /etc/apt/sources.list.d/mono-official-stable.list \
    && echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null \
    && apt-get update \
    && apt-get install -y \
    build-essential \
    curl \
    cmake \
    mono-devel \
    nuget \
    lsb-release \
    && apt-get clean \
    && nuget update -self \
    && rm -rf /tmp/NuGetScratch/lock/*
