FROM mcr.microsoft.com/windows/servercore:ltsc2019

LABEL version=1.1.1

# Restore the default Windows shell
SHELL ["powershell", "-command", "$ErrorActionPreference = 'Stop';"]

ADD https://aka.ms/vs/16/release/vs_buildtools.exe C:/TEMP/
RUN Start-Process -FilePath C:/TEMP/vs_buildtools.exe -ArgumentList "--quiet", "--norestart", "--wait", "--nocache", \
    "--add", "Microsoft.VisualStudio.Workload.VCTools", \
    "--add", "Microsoft.VisualStudio.Component.VC.ATL", \
    "--add", "Microsoft.VisualStudio.Component.VC.CMake.Project", \
    "--add", "Microsoft.VisualStudio.Component.VC.Tools.x86.x64", \
    "--add", "Microsoft.VisualStudio.Component.Windows10SDK.18362", \
    "--add", "Microsoft.VisualStudio.Workload.NetCoreBuildTools", \
    "--add", "Microsoft.Component.VC.Runtime.UCRTSDK" -Wait; \
    Remove-Item -Force -Recurse "C:/TEMP";

RUN Set-ExecutionPolicy Bypass -Scope Process -Force; iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex; \
    choco install git -y --params="'/GitAndUnixToolsOnPath /NoGitLfs /NoShellIntegration'"  --version 2.14.1; \
    choco install cmake -y --installargs 'ADD_CMAKE_TO_PATH=System' --version 3.15.5; \
    choco install nuget.commandline -y --version 4.3.0;

CMD ["powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]