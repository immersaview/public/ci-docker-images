FROM centos:centos7.4.1708

LABEL version=1.1.3

## Developer Toolset 7 from Software Collections (https://www.softwarecollections.org/en/scls/rhscl/devtoolset-7/)
RUN rpm --import "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF"; \
    curl https://download.mono-project.com/repo/centos7-stable.repo | tee /etc/yum.repos.d/mono-centos7-stable.repo; \
    yum install -y --setopt=tsflags=nodocs \
        mono-devel \
        nuget \
        epel-release \
        redhat-lsb \
        centos-release-scl; \
    # separate install as these requires centos-release-scl installed first
    yum install -y --setopt=tsflags=nodocs \
        cmake3 \
        devtoolset-7; \
    yum clean all -y; \
    rm -rf /var/cache/yum; \
    ln -s /usr/bin/cmake3 /usr/bin/cmake; \
    ln -s /usr/bin/cpack3 /usr/bin/cpack; \
    nuget update -self; \
    echo "source scl_source enable devtoolset-7" >> /etc/bashrc;

# Enable the SCL for all bash scripts.
ENV BASH_ENV="/usr/bin/scl_enable.sh" \
    ENV="/usr/bin/scl_enable.sh" \
    PROMPT_COMMAND=". /usr/bin/scl_enable.sh"

COPY centos7 /

SHELL [ "/usr/bin/env", "bash", "-c" ]
ENTRYPOINT ["/usr/bin/centos7-entrypoint.sh"]
CMD ["/usr/bin/env", "bash"]
