FROM ubuntu:bionic-20220128

LABEL version=1.6.0

## Setting the SHELL environment variable as this is what flock uses for it's commands internally.
## This is normally set on Ubuntu machines.  Using the SHELL docker cmd or other methods has not worked.
ENV SHELL="/bin/bash"

## nuget leaves a lock file, which is what the line 'rm -rf /tmp/NuGetScratch/lock/*' removes.
RUN apt-get update \
    && apt-get install -y gnupg apt-transport-https ca-certificates \
    && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF \
    && echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | tee /etc/apt/sources.list.d/mono-official-stable.list \
    && apt-get update \
    && apt-get install -y \
    build-essential \
    curl \
    cmake \
    mono-devel \
    nuget \
    lsb-release \
    && apt-get clean \
    && nuget update -self \
    && rm -rf /tmp/NuGetScratch/lock/*
