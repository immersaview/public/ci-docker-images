FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.3-centos7

LABEL version=1.1.0

RUN yum install -y --setopt=tsflags=nodocs \
      yasm \
      zlib-devel \
    && yum clean all -y \
    && rm -rf /var/cache/yum
