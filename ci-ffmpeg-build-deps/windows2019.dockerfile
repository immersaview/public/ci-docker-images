FROM mcr.microsoft.com/windows/servercore:ltsc2019

LABEL version=1.1.0

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

ADD https://aka.ms/vs/16/release/vs_buildtools.exe C:/TEMP/
RUN Start-Process -FilePath C:/TEMP/vs_buildtools.exe -ArgumentList '--quiet', '--norestart', '--wait', '--nocache', \
    '--add', 'Microsoft.VisualStudio.Workload.VCTools', \
    '--add', 'Microsoft.VisualStudio.Component.VC.Tools.x86.x64', \
    '--add', 'Microsoft.VisualStudio.Component.Windows10SDK.18362', \
    '--add', 'Microsoft.Component.VC.Runtime.UCRTSDK' -Wait -NoNewWindow; \
    Remove-Item -Force -Recurse "C:/TEMP";

ADD "https://github.com/msys2/msys2-installer/releases/download/2021-06-04/msys2-base-x86_64-20210604.sfx.exe" C:/TEMP/msys2-base.exe
RUN C:/TEMP/msys2-base.exe -y; \
    Remove-Item -Force -Recurse "C:/TEMP"; \
    C:/msys64/usr/bin/bash.exe -l -c '# Initialise first login'; \
    C:/msys64/usr/bin/bash.exe -l -c 'pacman --sync --sysupgrade --refresh --noconfirm'; \
    C:/msys64/usr/bin/bash.exe -l -c 'pacman --sync --refresh --noconfirm \
       diffutils \
       make \
       nasm \
       yasm \
    '; \
    C:/msys64/usr/bin/bash.exe -l -c 'pacman --sync --clean --noconfirm';
