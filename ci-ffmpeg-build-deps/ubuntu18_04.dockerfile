FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04

LABEL version=1.2.0

RUN apt-get update \
    && apt-get install -y \
      yasm \
      zlib1g-dev \
    && apt-get clean
