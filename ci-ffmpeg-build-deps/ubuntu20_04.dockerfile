FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.0.0-ubuntu20_04

LABEL version=1.0.0

RUN apt-get update \
    && apt-get install -y \
      yasm \
      zlib1g-dev \
    && apt-get clean
