# ci-ffmpeg-build-deps

A docker build environment for compiling FFmpeg.

## Windows
[Visual Studio/MSYS2 shell setup](https://ffmpeg.org/platform.html#Microsoft-Visual-C_002b_002b-or-Intel-C_002b_002b-Compiler-for-Windows) is intentionally left to the user so that build scripts can be used in multiple build environments.
