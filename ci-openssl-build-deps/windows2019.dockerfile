FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.1-windows2019

LABEL version=1.0.0

# Restore the default Windows shell
SHELL ["powershell", "-command", "$ErrorActionPreference = 'Stop';"]

# Remove the existing perl.exe on the path which exists in the git bin folder.
# This is to avoid calling the wrong perl.exe being from our build scipts.
RUN Remove-Item (Get-Command perl).Source; \
    choco install strawberryperl -y --version 5.24.2.1;

CMD ["powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]
