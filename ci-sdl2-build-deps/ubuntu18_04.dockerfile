FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04

LABEL version=1.2.0

RUN set -e; \
apt-get update; \
    apt-get install -y \
        libx11-dev \
        libx11-xcb-dev \
        libxcomposite-dev \
        libasound2-dev \
        libpulse-dev \
        libxss-dev \
        libxrandr-dev \
        libxcursor-dev \
        libglu1-mesa-dev \
        mesa-common-dev \
        # These files are required by Viewer (cef)
        libnss3-dev \
        libxi-dev \
        libxtst6 \
        libpango-1.0-0 \
        libpangocairo-1.0-0 \
        libatk1.0-0; \
    apt-get clean;
