FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.3-centos7

LABEL version=1.0.0

RUN set -e; \
    yum install -y \
        libX11-devel \
        alsa-lib-devel \
        pulseaudio-libs-devel \
        mesa-libGLU-devel \
        mesa-libGL-devel \
        libXrandr-devel \
        libXScrnSaver-devel; \
    yum clean all;
