param(
    $ErrorActionPreference = "Stop",
    $Mode="dry-run"
)

# Required for docker manifest command
$env:DOCKER_CLI_EXPERIMENTAL="enabled"

docker login -u "${env:CI_REGISTRY_USER}" -p "${env:CI_REGISTRY_PASSWORD}" "${env:CI_REGISTRY}"

function Get-LocalImageVersion($dockerfile) {
   $result = Select-String -Path ($dockerfile).FullName -Pattern "LABEL version=(.*)"

   # Groups[0] is the whole match, so the group at index 1 is what we want
   if (($result).Matches.Groups.Count -gt 1) {
       ($result).Matches.Groups[1].Value
   } else {
       ""
   }
}

$Dockerfiles = Get-ChildItem -Path ./ -Recurse -Filter "win*.dockerfile"

try {
    foreach ($Dockerfile in $Dockerfiles){
        $ImagePath = ($Dockerfile).FullName
        $ImageName = ($Dockerfile).Directory.Name
        $ImageSuffix = ($Dockerfile).Basename
        $ImageVersion = Get-LocalImageVersion $Dockerfile

        if ($ImageSuffix -ne "Dockerfile") {
            $ImageSuffix = "-${ImageSuffix}"
        } else {
            $ImageSuffix = ""
        }

        if (!$ImageVersion) {
            Write-Error "Version label is required for image: ${ImageName}. Skipping building image"
            continue
        }

        $ImageTag = "${ImageVersion}${ImageSuffix}"
        $BuildImageName = "${env:CI_REGISTRY_IMAGE}/${ImageName}:${ImageTag}"

        if (docker manifest inspect $BuildImageName) {
            Write-Output ""
            Write-Output "======================================================================================"
            Write-Output "Skipping build of ${ImageName}:${ImageTag} since remote image already exists"
            Write-Output "======================================================================================"
            Write-Output ""
        } else {
            Write-Output ""
            Write-Output "======================================================================================"
            Write-Output "Build ${BuildImageName}"

            if ("${Mode}" -eq "publish") {
                docker build --tag "${BuildImageName}" --file "${ImagePath}" "${ImageName}/"
                if (${LastExitCode} -ne 0) { Write-Error 'Failed to build docker image' }
                docker push "${BuildImageName}"
                if (${LastExitCode} -ne 0) { Write-Error 'Failed to push docker image' }
            } else {
                docker build --no-cache --tag "${BuildImageName}" --file "${ImagePath}" "${ImageName}/"
                if (${LastExitCode} -ne 0) { Write-Error 'Failed to build docker image' }
            }

            Write-Output "======================================================================================"
            Write-Output ""
        }
    }
}
finally {
    docker logout "${env:CI_REGISTRY}"
}

