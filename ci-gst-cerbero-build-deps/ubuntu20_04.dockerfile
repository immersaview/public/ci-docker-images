FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.0.0-ubuntu20_04

LABEL version=1.0.0

RUN set -e; \
    apt-get update; \
    apt-get install -y \
        git \
        python3 \
        python3-setuptools \
        sudo; \
    apt-get clean; \
    git config --global user.email "it@immersaview.com"; \
    git config --global user.name "ImmersaView";
