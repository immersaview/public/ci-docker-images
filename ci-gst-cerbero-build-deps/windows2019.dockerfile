# escape=` (backtick)

# This dockerfile is adapted from the GStreamer windows dockerfile:
#   https://gitlab.freedesktop.org/gstreamer/gst-ci/tree/master/docker/windows

FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.1-windows2019

LABEL version=1.1.0

### ----- Copied from https://gitlab.freedesktop.org/gstreamer/gst-ci/tree/32176795a78967380744b8da283874d6fd79ab6d/docker/windows -----
# Make sure any failure in PowerShell scripts is fatal
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop';"]
ENV ErrorActionPreference='Stop'

RUN $python_dl_url = 'https://www.python.org/ftp/python/3.7.5/python-3.7.5.exe'; `
    Write-Host "Installing Python"; `
    Invoke-WebRequest -Uri $python_dl_url -OutFile C:\python3-installer.exe; `
    Start-Process C:\python3-installer.exe -ArgumentList '/quiet InstallAllUsers=1 PrependPath=1 TargetDir=C:\Python37' -Wait; `
    Remove-Item C:\python3-installer.exe -Force;

RUN choco uninstall -y git; `
    $git_url = 'https://github.com/git-for-windows/git/releases/download/v2.24.1.windows.2/MinGit-2.24.1.2-64-bit.zip'; `
    Write-Host "Installing Git"; `
    Invoke-WebRequest -Uri $git_url -OutFile C:\mingit.zip; `
    Expand-Archive C:\mingit.zip -DestinationPath c:\mingit; `
    Remove-Item C:\mingit.zip -Force; `
    # Add git to the path
    $env:PATH = [System.Environment]::GetEnvironmentVariable('PATH', 'Machine') + ';' + 'c:\mingit\cmd'; `
    [Environment]::SetEnvironmentVariable('PATH', $env:PATH, [EnvironmentVariableTarget]::Machine);

RUN $zip_url = 'https://www.7-zip.org/a/7z1900-x64.exe'; `
    Write-Host "Installing 7zip"; `
    Invoke-WebRequest -Uri $zip_url -OutFile C:\7z-x64.exe; `
    Start-Process C:\7z-x64.exe -ArgumentList '/S /D=C:\7zip\' -Wait; `
    Remove-Item C:\7z-x64.exe -Force;

# The Invoke-WebRequest below fails most of the time, so just copy the files in for now.
COPY mingw-get-0.6.3-mingw32-pre-20170905-1-bin.tar.xz C:\mingw-get.tar.xz
# RUN $msys_mingw_get_url = 'https://osdn.net/frs/redir.php?m=aarnet&f=mingw%2F68260%2Fmingw-get-0.6.3-mingw32-pre-20170905-1-bin.tar.xz'; `
#    Write-Host "Downloading and extracting mingw-get for MSYS"; `
#    Invoke-WebRequest -Uri $msys_mingw_get_url -OutFile C:\mingw-get.tar.xz; `
#    C:\7zip\7z e C:\mingw-get.tar.xz -o"C:\\"; `
RUN C:\7zip\7z e C:\mingw-get.tar.xz -o"C:\\"; `
    C:\7zip\7z x C:\mingw-get.tar -o"C:\\MinGW"; `
    Remove-Item C:\mingw-get.tar.xz -Force; `
    Remove-Item C:\mingw-get.tar -Force; `
    Write-Host "Installing MSYS for Cerbero into C:/MinGW using mingw-get"; `
    Start-Process C:\MinGW\bin\mingw-get.exe -ArgumentList 'install msys-base mingw32-base mingw-developer-toolkit' -Wait; `
    $env:PATH = 'c:\MinGW\bin' + ';' + [System.Environment]::GetEnvironmentVariable('PATH', 'Machine'); `
    [Environment]::SetEnvironmentVariable('PATH', $env:PATH, [EnvironmentVariableTarget]::Machine);

COPY msys2-base-x86_64-20190524.tar.xz C:\msys2-x86_64.tar.xz
# RUN $msys2_url = 'https://downloads.sourceforge.net/project/msys2/Base/x86_64/msys2-base-x86_64-20190524.tar.xz?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fmsys2%2Ffiles%2FBase%2Fx86_64%2Fmsys2-base-x86_64-20190524.tar.xz%2Fdownload%3Fuse_mirror%3Djaist&ts=1578014266'; `
#     Write-Host "Installing MSYS2 into C:/msys64"; `
#     Invoke-WebRequest -Uri $msys2_url -OutFile C:\msys2-x86_64.tar.xz; `
#     C:\7zip\7z e C:\msys2-x86_64.tar.xz -o"C:\\"; `
RUN C:\7zip\7z e C:\msys2-x86_64.tar.xz -o"C:\\"; `
    C:\7zip\7z x C:\msys2-x86_64.tar -o"C:\\"; `
    Remove-Item C:\msys2-x86_64.tar.xz -Force; `
    Remove-Item C:\msys2-x86_64.tar -Force; `
    Remove-Item C:\7zip -Recurse -Force;

RUN Write-Host "Installing Choco"; `
    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')); `
    refreshenv;

RUN Write-Host "Installing git-lfs"; `
    choco install -y git-lfs; `
    refreshenv;

RUN Write-Host "Installing Meson"; `
    pip install meson;

COPY msys-shell.bat C:\

RUN C:\Windows\Syswow64\cmd.exe /C C:\msys-shell.bat 'git config --global user.email it@immersaview.com'; `
    C:\Windows\Syswow64\cmd.exe /C C:\msys-shell.bat 'git config --global user.name ImmersaView'; `
    C:\Windows\Syswow64\cmd.exe /C C:\msys-shell.bat 'git config --global core.autocrlf false';
