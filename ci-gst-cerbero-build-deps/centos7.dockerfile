FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.3-centos7

LABEL version=1.1.0

RUN set -e; \
    # `yum check-update` returns 100 if there are packages to update.
    # Check this since it is a non-error.
    yum check-update || [[ $? = 100 || $? = 0 ]]; \
    yum install -y \
        git \
        python3-3.6.8-10.el7.x86_64 \
        python3-setuptools-39.2.0-10.el7.noarch \
        sudo; \
    yum clean all; \
    rm -rf /var/cache/yum; \
    git config --global user.email "it@immersaview.com"; \
    git config --global user.name "ImmersaView";
