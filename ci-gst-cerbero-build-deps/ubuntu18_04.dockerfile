FROM registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.6.0-ubuntu18_04

LABEL version=1.3.0

RUN set -e; \
    apt-get update; \
    apt-get install -y \
        git \
        python3=3.6.7-1~18.04 \
        python3-setuptools=39.0.1-2 \
        sudo; \
    apt-get clean; \
    git config --global user.email "it@immersaview.com"; \
    git config --global user.name "ImmersaView";
