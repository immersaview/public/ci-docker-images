rem Usage:
rem C:\Windows\Syswow64\cmd.exe /C C:\msys-shell.bat

rem This batch file sets up a specific MinGW environment used for building GStreamer.
rem It is intended to be a CLI alternative to C:\MinGW\msys\1.0\msys.bat, which seems to be intended for a GUI desktop environment.

rem The values of these variables was set to values that C:\MinGW\msys\1.0\msys.bat set on a Windows 10 Enterprise Eval VM.
set WD=C:\MinGW\msys\1.0\bin
set MSYSCON=sh.exe
set MSYSTEM=MINGW32

C:\MinGW\msys\1.0\bin\sh --login -c %1
